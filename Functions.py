import json
import random
import sys
import datetime

users = json.load(open("user.json", "r"))
words = json.load(open("words.json", "r"))
stats=json.load(open("stats.json","r"))
scores = json.load(open("scores.json", "r"))

for k, v in users.items():
    id = len(users.keys())
    new_id = id + 1

#--------------------------------------------------login----------------------------------------------------------------


def login(username,password):
    found=False

    for k, v in users.items():
        if (username == v['Username']) and (password == v['Password']):
            found = True

            if k in scores:
                rezultati = "logged2"
                break
            else:
                rezultati="logged"
                break

        else:

            rezultati="gabim"


    return rezultati

#--------------------------------------------------start----------------------------------------------------------------



def start():
    p = raw_input()
    invalid_input = True
    if (p == 'y' or p == 'Y'):
        createUser()
        invalid_input = False

    elif (p == 'n' or p == 'N'):
        print("Goodbye!")
        invalid_input = False

    else:
        print("The given input is not understandable. Please write Y for yes and N for no.")
        start()
    while invalid_input: True





#--------------------------------------------------createUser-----------------------------------------------------------


def createUser():
    username = raw_input("Username:")
    passw = raw_input("Password:")
    pass2 = raw_input("Confirm Password:")
    name = raw_input("Name:")
    surname = raw_input("Surname:")
    email = raw_input("Email:")

    with open('user.json', 'r') as f:
        file = json.load(f)


        invalid=False
        while invalid:False
        if(username=="" or passw=="" or pass2=="" or name=="" or surname=="" or email==""):
            print "Please write down all the required data!"
            createUser()

        else:

            exists=checkUserName(username)

            if(exists=="yes"):
               createUser()
            elif(passw!=pass2):
                print "Passwords do not match! Please try again!\n"
                createUser()
            else:
                file[new_id] = {
                    "Username": username,
                    "Password": passw,
                    "Name": name,
                    "Surname": surname,
                    "Email": email
                }
                invalid = True

                with open('user.json', 'w') as f:
                    json.dump(file, f, indent=1)
                    print "User {0} created!".format(username)

#------------------------------------------------getLastSignIn----------------------------------------------------------


def getLastSignIn(uid):
    for k, v in scores.items():
        new_k = int(k)
        if(new_k==uid):
            sign_last=max(scores[k]['Last signed in'])

    return sign_last

#--------------------------------------------------updateSignIn---------------------------------------------------------

def updateSignIn(userid):
    for key, value in scores.iteritems():
        new_key = int(key)
        if (new_key == userid):

            last_date = getLastSignIn(userid)
            now = datetime.datetime.now()
            new_date=now.strftime("%Y-%m-%d %I:%M:%S")
            scores[key]['Last signed in'].append(new_date)
    with open('scores.json', 'w') as outfile:
        json.dump(scores, outfile, indent=1)

        return scores
#--------------------------------------------------getUserId------------------------------------------------------------
def getUsername(userId):
    for k, v in users.items():
        new_k=int(k)
        if(new_k==userId):
            user=v['Username']
            return user


#-------------------------------------------------checkUserName---------------------------------------------------------
def checkUserName(username):
    for k, v in users.items():
        if (username == v['Username']):

            rezultati="Username taken!Pick another one!\n"
            print rezultati
            res="yes"
            break
        else:
            res="no"


    return res


# ---------------------------------------------------startGame----------------------------------------------------------

def startGame(userId):
    print("Please pick a category! \n-Animals \n-Cars")
    category = raw_input()
    category = category.lower()
    invalid_input = True
    if (category == "animals"):
        print("Please choose difficulty level! \n-Easy\n-Medium\n-Hard")
        level = checkLevel()
        if (level == "easy"):
            hidden_word = (random.choice(words['Animals_EASY']))
            lines = len(hidden_word) * "__ "
            print(lines)
            guessLetter(hidden_word,level,userId)

        elif (level == "medium"):
            hidden_word = (random.choice(words['Animals_MEDIUM']))
            lines = len(hidden_word) * "__ "
            print(lines)
            guessLetter(hidden_word,level,userId)
        else:
            hidden_word = (random.choice(words['Animals_HARD']))
            lines = len(hidden_word) * "__ "
            print(lines)
            guessLetter(hidden_word,level,userId)

    elif (category == "cars"):

        print("Please choose difficulty level! \n-Easy\n-Medium\n-Hard")
        level = checkLevel()
        if (level == "easy"):
            hidden_word = (random.choice(words['Cars_EASY']))
            lines = len(hidden_word) * "__ "
            print(lines)
            guessLetter(hidden_word,level,userId)

        elif (level == "medium"):
            hidden_word = (random.choice(words['Cars_MEDIUM']))
            lines = len(hidden_word) * "__ "
            print(lines)
            guessLetter(hidden_word,level,userId)
        else:
            hidden_word = (random.choice(words['Cars_HARD']))
            lines = len(hidden_word) * "__ "
            print(lines)
            guessLetter(hidden_word,level,userId)
    else:
        print ("Wrong category! Please pick animals or cars!")
        startGame(userId)
    while invalid_input: True


# --------------------------------------------------level---------------------------------------------------------------

def checkLevel():
    while True:
        level = raw_input("Level:")
        level = level.lower()
        if (level == "easy"):
            return level
        elif (level == "medium"):
            return level
        elif (level == "hard"):
            return level
        else:
            print("Wrong level entered! Please choose easy, medium or hard level")
            continue

        checkLevel()


#-------------------------------------------------guessLetter-----------------------------------------------------------

def guessLetter(hidden_word,level,userId):

    guessed=[]
    missed=[]
    used = []
    count = 7
    while count != 0:
        userInput = raw_input("Please guess a letter!")
        userInput=userInput.lower()
        if len(userInput) !=1:
            print "You can enter only one character!"
        elif userInput.isalpha()==False:
            print "You must choose only letters!"
        elif userInput in used:
                    print ("You have already used this letter, please try again.")
                    continue
        elif userInput in hidden_word:
            print ("Correct!")
            guessed.append(userInput)
            used.append(userInput)
            lines = " ".join(c if c in guessed else "__" for c in hidden_word)
            print lines


            foundAllLetters = True
            for i in range(len(hidden_word)):
                if hidden_word[i] not in lines:
                    foundAllLetters = False
                    break
            if foundAllLetters:
                count=0
                result ='Win'
                endGame(result,hidden_word,level, userId)

        else:
            count = count-1
            missed.append(userInput)
            used.append(userInput)
            print "Incorrect! You have {0} attempts left!\nMissed letters:".format(count)
            print (', '.join(missed))

            if count==6:

                print """
                _____________
                 |        |
                 |        
                 |       
                 |        
                 |       
               __|_______                                                                                                                           
                                            
                """

            elif count==5:
                print """
                _____________
                 |        |
                 |        O
                 |       
                 |        
                 |       
               __|_______                                                                                                                           
                                            
                """


            elif count == 4:
                print """
                _____________
                 |        |
                 |        O
                 |        |
                 |        |
                 |       
               __|_______                                                                                                                           
                                            
                """

            elif count==3:
                print """               
                _____________
                 |        |
                 |        O
                 |       \|
                 |        |
                 |       
               __|_______                                                                                                                           
                                            
                """


            elif count==2:
                print """                 
                _____________
                 |        |
                 |        O
                 |       \|/
                 |        |
                 |       
               __|_______                                                                                                                           
                                            
                """

            elif count==1:
                print """ 
                _____________
                 |        |
                 |        O
                 |       \|/
                 |        |
                 |       /
               __|_______                                                                                                                           
                                            
                """
            else:
                print"Game over"
                print """                 
                _____________
                 |        |
                 |        O
                 |       \|/
                 |        |
                 |       / \ 
               __|_______                                                                                                                           

                    """

                endGame("lose",hidden_word,level,userId)






#------------------------------------------------endGame()--------------------------------------------------------------
def endGame(result, word,level,userId):
    scores = json.load(open("scores.json", "r"))

    max_score=getMaxPoints(userId,scores)
    if result != 'lose':

        for key, value in scores.iteritems():
            new_key = int(key)

            if (new_key == userId):

                max_score = max(scores[key]['Scores'])


                new_score = calculatePoints(level, max_score)
                scores[key]['Scores'].append(new_score)
        with open('scores.json', 'w') as outfile:
            json.dump(scores, outfile, indent=1)
        print "Congratulations, you win!"
        print "You correctly guessed the word '%s'!" % word
        final_score=getMaxPoints(userId,scores)
        print "You have %s points!" %final_score


    else:
        print "You lost! The hidden word was '%s'." % word
        print "Your highest score is %s" %max_score



    while True:
        play_again = raw_input("Play again? [y/n] ")
        if 'y' in play_again.lower():
            startGame(userId)
        elif 'n' in play_again.lower():
            user=getUsername(userId)

            print "See you next time {0}.".format(user)
            sys.exit(0)
        else:
            print "Wrong input?"
#------------------------------------------------calculatePoints--------------------------------------------------------

def calculatePoints(level,max_score):

    if level == "easy":
        points = 1
        score = max_score + points
        return score
    elif level == "medium":
        points = 3
        score = max_score + points
        return score
    else:
        points = 5
        score = max_score + points
        return score

#-------------------------------------------------maxPoints-------------------------------------------------------------
def getMaxPoints(userId,scores):

    for k,v in scores.items():
        new_k=int(k)
        if(new_k==userId):
            scorelist=scores[k]['Scores']
            if(len(scorelist)>1):
                max_score=max(scores[k]['Scores'])
                return max_score
            else:
                max_score=scorelist
                return max_score
                break

#--------------------------------------------------getStats-------------------------------------------------------------
def getStats():
    total = []

    for key, val in scores.iteritems():
        new_scores = val['Scores']
        total = total + new_scores

    maxScore = max(total)
    count = len(total)

    total_sum = 0

    for score in total:
        total_sum += int(score)

    if count != 0:
        average = round(float(total_sum) / count, 1)
    else:
        average = 0

    stats = {"High score": maxScore, "Games played": count, "Average score": average}

    print "The current game stats: {0} ".format(stats)
    with open('stats.json', 'w') as f:
        json.dump(stats, f, indent=1)

#--------------------------------------------------printStats-----------------------------------------------------------
